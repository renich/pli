Pianoteq Linux Installer
========================

Description
-----------
This isntaller presents a user-friendly way of installing Pianoteq.

Instructions
------------
First, you want to make sure you have the tools the script requires:

.. code:: bash

    # install 7z and desktop-file-utils (GNOME)
    dnf -y install p7zip desktop-file-utils

Now, you want to download the icon you will be using for Pianoteq in your apps overlay. I like this one: https://images.app.goo.gl/exorghV98CMXbZdE6

Place it on this directory (PLI's directory) and name it: `pianoteq.png`.

After that, you just need to run the script and point it to your, already downloaded, pianoteq 7z file:

.. code:: bash

    ./pli ~/Downloads/pianoteq_linux_v705.7z

That's it. It will ask you your sudo password the first time. Then, it will just install it and you will have the Icon available in
the ``Activities > Show applications > All`` overlay.

.. image:: screenshot.png
   :width: 50%
   :align: center

Updating
--------
For updating, just run the script again, pointing it to the new 7z file. The script will remove the old installation and install the
new one. I've tried it many times and it works well. Your presets are safe and all.

